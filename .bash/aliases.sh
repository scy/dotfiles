# Disable the long compile options (etc.) banner at the top of every ffmpeg (and
# related tools) invocation.
alias ffmpeg='ffmpeg -hide_banner'
alias ffplay='ffplay -hide_banner'
alias ffprobe='ffprobe -hide_banner'
