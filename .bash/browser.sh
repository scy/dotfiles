if [ -n "$DISPLAY" ]; then
	# If this is X11 (or Wayland with Xwayland), set $BROWSER to xdg-open.
	if command -v xdg-open >/dev/null 2>&1; then
		export BROWSER='xdg-open'
	fi
else
	# Check whether one of the popular browsers is available and use that.
	# I have not yet really decided on my preference, therefore this list is
	# simply an alphabetical one.
	for cand in elinks links2 links lynx w3m; do
		if command -v "$cand" >/dev/null 2>&1; then
			export BROWSER="$cand"
			break
		fi
	done
fi
