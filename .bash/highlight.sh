#!/bin/sh

# highlight <http://andre-simon.de/doku/highlight/en/highlight.php> defaults.
export HIGHLIGHT_OPTIONS='-O truecolor -s zenburn --wrap-no-numbers -t 4'

alias hl='highlight'
alias hll='hl -l'  # with line numbers
