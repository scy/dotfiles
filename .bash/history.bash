# Don't store lines starting with a space in the history, or lines identical to the one before.
HISTCONTROL='ignorespace:ignoredups'

# Please let me have a huge history size. RAM is cheap.
HISTSIZE='100000'

# Store timestamps in history file, and display them as 'Mon 2020-06-01 23:42:05'.
HISTTIMEFORMAT='%a %Y-%m-%d %H:%M:%S  '

# Append to the history instead of overwriting the file.
shopt -s histappend
