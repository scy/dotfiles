# Default locale is US English. This sets my preference for UI text (see
# LANGUAGE below though) as well as date and time formats, collations, etc.
# <https://www.gnu.org/software/gettext/manual/html_node/Locale-Environment-Variables.html>
export LANG='en_US.UTF-8'

# Preference list of UI languages. Also used by Electron apps to determine spell
# checking dictionaries. Signal, for example, will display the UI in US English
# and accept English and German spelling. "C" is required because else all the
# normal command-line tools start speaking German to me. I have not yet
# completely understood why; apparently "en" isn't a valid language for them,
# but "C" ("no translation at all") works. Signal still needs "en" to be listed
# as well, though.
# $LANGUAGE is documented here:
# <https://www.gnu.org/software/gettext/manual/html_node/The-LANGUAGE-variable.html>
export LANGUAGE='en_US:en:C:de_DE:de'
