source_if_exists() {
	if [ -e "$1" ]; then
		. "$1"
	fi
}

source_if_exists "$HOME/.nix-profile/etc/profile.d/nix.sh"
source_if_exists "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh"
