# Setting up my prompt & terminal title.

# Note that I'm using $HOSTNAME instead of \h so that it can be overridden.

# Get current cursor row, based on <https://unix.stackexchange.com/a/183121>.
# Note that this is bash-only (non-POSIX) because of the flags to `read`.
cursor_row() {
	IFS=';' read -sdR -p $'\E[6n' row col
	echo "${row#*[}"
}

# Helper function to convert 100000 into 1d3h46m40s.
duration() {
	local seconds="$1"
	local days="$((seconds / 86400))"
	seconds="$((seconds - 86400 * days))"
	local hours="$((seconds / 3600))"
	seconds="$((seconds - 3600 * hours))"
	local minutes="$((seconds / 60))"
	seconds="$((seconds - 60 * minutes))"
	local result=''
	if [ "$days" -gt 0 ]; then
		result="$result${days}d"
	fi
	if [ "$hours" -gt 0 ]; then
		result="$result${hours}h"
	fi
	if [ "$minutes" -gt 0 ]; then
		result="$result${minutes}m"
	fi
	# Always end with "Xs", even if it generates something like "3h0s". This is
	# on purpose to communicate the accuracy.
	result="$result${seconds}s"
	printf '%s\n' "$result"
}

# Tracks the number of space characters that will need to be inserted before
# a `trap DEBUG` timestamp to align it below the PS1 timestamp.
TRAP_DEBUG_SPACES=0
# Track the time when the command started.
_cmd_starts() {
	# Since the DEBUG trap gets called multiple times (not just once per
	# command), only update LAST_CMD_STARTED if it's not set yet. It will be
	# cleared by _ps1_dyn before displaying the prompt.
	if [ -n "$LAST_CMD_STARTED" ]; then
		return
	fi
	LAST_CMD_STARTED="$SECONDS"

	# If more than a minute has passed since the prompt was displayed, output the
	# current time so I can see when the command was actually started.
	if [ "$((SECONDS - LAST_PROMPT_SHOWN))" -ge 60 ]; then
		local updated_time='\[\e[0;37;40m\]\[\e[30;47m\]\t\[\e[37;40m\]\[\e[0m\]\n'
		if [ "$TRAP_DEBUG_SPACES" -ne 0 ]; then
			# This is a fancy way of repeating a string in shell scripts.
			printf ' %.0s' $(seq 1 "$TRAP_DEBUG_SPACES")
		fi
		printf '%s' "${updated_time@P}"
	fi
}
trap _cmd_starts DEBUG

# This function will prepare the dynamic parts of the prompt.
_ps1_dyn() {
	local rc="$?"
	TRAP_DEBUG_SPACES=0

	# Empty line (unless we're at the top of the screen).
	PS1_EMPTY_LINE='\n'
	if [ "$(cursor_row)" -eq 1 ]; then
		PS1_EMPTY_LINE=''
	fi

	# Display a white-on-red badge containing $? if it isn't zero.
	PS1_RC=''
	if [ "$rc" -ne 0 ]; then
		PS1_RC='\[\e[97;41m\] '"$rc"' '
		TRAP_DEBUG_SPACES=$((TRAP_DEBUG_SPACES + 2 + ${#rc}))
	fi

	# Runtime of last command.
	PS1_DURATION=''
	if [ -n "$LAST_CMD_STARTED" ]; then
		local duration="$((SECONDS - LAST_CMD_STARTED))"
		# Only display duration if it was at least 3 seconds.
		if [ "$duration" -ge 3 ]; then
			local duration_str="$(duration $duration)"
			PS1_DURATION='\[\e[0m\e[38;5;238m\]\[\e[0m\e[48;5;238m\]'" $duration_str "'\[\e[0m\]'
			TRAP_DEBUG_SPACES=$((TRAP_DEBUG_SPACES + 3 + ${#duration_str}))
		fi
	fi
	# Reset, so that _cmd_starts will set it again.
	unset LAST_CMD_STARTED

	# Keep track of when the prompt was last displayed.
	LAST_PROMPT_SHOWN="$SECONDS"
}

# Function that builds the prompt from pieces, for better legibility. Note that
# this will only run _once_ when evaluating this file, and not for every prompt.
_ps1() {
	# Notify compatible terminal emulators that a prompt starts here and that we
	# won't redraw it on window resize, so the terminal emulator should not
	# remove it.
	local bop='\[\e]133;A;redraw=0\e\\\]'

	# Terminal title (and style reset).
	local title='$SHORTHOSTNAME:\W'
	[ "$USER" == 'scy' ] || title="\$USER@$title"
	title='\[\e]2;'"$title"'\e\\\e[0m\]'

	# Current time.
	local ts='\[\e[30;47m\] \t \[\e[0m\]'

	# Background uses 256-color mode. If the terminal does not support it,
	# the prompt should degrade gracefully to having no background at all.
	local start='\[\e[0m\e[48;5;238m\] '

	# user@host.
	local user_at_host='\u\[\e[30m\]@\[\e[39m\]$SHORTHOSTNAME'

	# Current working directory.
	local curdir='\[\e[93m\]\w'

	# End of prompt.
	local end=' \[\e[49;38;5;238m\] \[\e[0;1;38;5;78m\]'

	local combined="$bop$title\${PS1_EMPTY_LINE@P}\${PS1_DURATION@P}\${PS1_RC@P}$ts$start$user_at_host $curdir$end"
	printf '%s' "$combined"
}
PROMPT_COMMAND='_ps1_dyn'
PS1="$(_ps1)"

# Reset colors before running the actual command, and signal to the terminal
# emulator that the command starts now.
PS0='\[\e[0m\e]133;C\e\\\]'

# Also tag the secondary prompt for the terminal emulator.
PS2='\[\e]133;A;k=s\e\\\e[0;49;37m\] \[\e[1;38;5;78m\]'
