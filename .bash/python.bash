# Load this file when running Python in interactive mode. It does some imports
# and prettyfies the output.
export PYTHONSTARTUP="$HOME/.config/python-repl-init.py"

# Workaround for Poetry trying to create/access a keyring, see
# <https://github.com/python-poetry/poetry/issues/1917> for details.
alias poetry='PYTHON_KEYRING_BACKEND=keyring.backends.fail.Keyring poetry'
