#!/bin/sh

p() {
	if [ "$#" -eq 0 ]; then
		python3
	else
		term-preview "$@"
	fi
}
