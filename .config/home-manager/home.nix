{ config, pkgs, ... }:

{
  # I could probably use `builtins.getEnv "USER"` (and `"HOME"`) here, but then
  # I'd have to rune home-manager in `--impure` mode. For now, I'll leave this
  # hard-coded until I run into a machine where the values are actually
  # different.
  home.username = "scy";
  home.homeDirectory = "/home/scy";

  home.stateVersion = "23.11";

  # Packages to install.
  home.packages = with pkgs; [
    neovim
  ];

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
