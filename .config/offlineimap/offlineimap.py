import subprocess


def bitwarden_get(what, item):
    return subprocess.check_output(
        ["bw", "--nointeraction", "get", what, item],
    ).strip()
