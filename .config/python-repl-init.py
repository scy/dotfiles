import os
import sys


try:
    from rich import pretty
    pretty.install()
except Exception:
    # Probably an ImportError, but we simply ignore any exception.
    pass
