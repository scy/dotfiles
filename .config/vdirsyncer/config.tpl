# This file is just a template. ~/bin/calsync will replace the uppercase tokens
# into their actual values by retrieving them from the password manager.

[general]
status_path = "~/.vdirsyncer/status/"

[pair gcal_scy]
a = "gcal_scy_remote"
b = "gcal_scy_local"
collections = COLLECTIONS_GCAL_SCY
conflict_resolution = "a wins"
partial_sync = "revert"
metadata = ["color"]

[storage gcal_scy_remote]
type = "google_calendar"
read_only = true
client_id = "GOOGLE_CLIENT_ID"
client_secret = "GOOGLE_CLIENT_SECRET"
token_file = "~/.vdirsyncer/gcal_scy.token"

[storage gcal_scy_local]
type = "filesystem"
path = "~/.calendars/google"
fileext = ".ics"
