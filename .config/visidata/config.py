# Nice Unicode separators. Should be the default nowadays anyway, but maybe not
# on older versions.
options.disp_column_sep = "│"
options.disp_keycol_sep = "┃"

# Show a nice bar in the histogram.
options.disp_histogram = "━"

# Hide the menu when not in use.
options.disp_menu = False

# Round the menu a bit.
options.disp_menu_boxchars = "││──╭╮╰╯├┤"

# Character for "pushes a new sheet".
options.disp_menu_push = "✦"

# Get rid of the black background (helps with transparent terminals).
options.color_default = ""
