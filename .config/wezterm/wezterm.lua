local wezterm = require 'wezterm'

if os.getenv('DISPLAY') then
	local default_prog = { 'bash' }
else
	local default_prog = { 'wsl', '-d', 'Ubuntu', '--cd', '~' }
end

return {
	-- Profiles
	default_prog = default_prog,

	-- Window Size
	initial_cols = 180,
	initial_rows = 45,

	-- Font
	font = wezterm.font 'Iosevka Term',
	font_size = 10.5,
	harfbuzz_features = { 'calt=0', 'clig=0', 'liga=0' },  -- disable ligatures

	-- Colors & Translucency
	color_scheme = 'Sihaya',
	-- WezTerm will _add_ this to the defaults, so no need for a separate Linux entry.
	color_scheme_dirs = { 'C:\\Users\\scy\\dotfiles\\.config\\wezterm\\colors' },
	bold_brightens_ansi_colors = false,  -- don't keep me from having bold text
	window_background_opacity = 0.95,
	-- Sihaya's normal background color at the bottom, pure black at the top.
	-- Gives the terminal a "tinted window" kind of look - or looks like really
	-- bad backlight bleed. You decide.
	window_background_gradient = {
		colors = { '#000000', '#242320' },
		orientation = 'Vertical',
		blend = 'LinearRgb',
	},

	-- Cursor
	-- Disabling cursor blink makes a _massive_ difference in GPU usage (20% vs 3%).
	-- Therefore, I'll disable it to save energy.
	cursor_blink_rate = 0,

	-- Tab Bar
	hide_tab_bar_if_only_one_tab = true,
	use_fancy_tab_bar = true,

	-- Visual Bell Only
	audible_bell = 'Disabled',
	visual_bell = {
		fade_in_duration_ms = 20,
		fade_out_duration_ms = 200,
		fade_in_function = 'Linear',
		fade_out_function = 'EaseOut',
	},

	-- Features
	enable_kitty_keyboard = true,  -- modernize keyboard input
}
