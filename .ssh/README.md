# scy's OpenSSH config

## Design goals

* Have as much as possible public, in [my dotfiles repo](https://codeberg.org/scy/dotfiles).
* Don't publicly expose the names of the hosts I connect to.
* Allow grouping the hosts, e.g. by project or customer or employer.
    * This also means that there should be a separate `known_hosts` file per group.
* Don't repeat yourself: Instead, write reusable config snippets.

## Basic file structure

* `config`: The top-level file `ssh` will read. Contains general (i.e. not host-specific) configuration, includes the private configs.
* `forward-ssh-agent.snip`: Config snippet to include on hosts that should have access to my SSH agent, e.g. to allow the remote host to access Git repositories protected via SSH authentication.
* `forward-gpg-<uid>.snip`: Config snippet to include on hosts that should have access to my GnuPG agent, e.g. to allow the remote host to sign Git commits.
    * Sadly, OpenSSH's `RemoteForward` directive does not support variables in the remote Unix socket path. Since the agent expects the socket at `/run/user/<uid>/gnupg/S.gpg-agent`, I need to have a version of this config for each of the remote UIDs I log in to.
    * TODO: Maybe it's possible to configure the remote agent to always look in a specific, fixed location, e.g. in my homedir.
* `authorized_keys`: SSH keys that are allowed to log in to a machine. This file is also in Git so that my main SSH key can log in everywhere I check out my dotfiles.
    * This is probably gonna become problematic once I want to add more keys only to certain hosts. I'll think about that once I have to.
* `known_hosts`: Host keys of hosts that I connect to and that I'm okay with disclosing publicly. This is basically only used to pre-populate keys for Codeberg, GitHub, GitLab etc.

And then there's the `private/` directory, which is not in Git, and contains the configuration and keys for hosts that I don't want to share publicly, grouped by project.
For each project, the following files exist:

* `private/<proj>.snip`: Snippet that will be `Include`d by each of the project's host configurations. It's mainly used to instruct OpenSSH to write the host key to a project-specific `.hosts` file, so it usually looks like this:

    ```sshconfig
    # PROJECT is obviously a placeholder for the project's name.
    UserKnownHostsFile ~/.ssh/private/PROJECT.hosts
    ```

* `private/<proj>.hosts`: The host keys for that project's hosts, as requested by the per-project `.snip` file explained above.
* `private/<proj>.conf`: The actual hosts and their configurations, as well as any forwarding to use. An example would look like this:

    ```sshconfig
    # Have all host names starting with PROJECT- include the project's snippet.
    Host PROJECT-*
        Include private/PROJECT.snip

    # Configuration for individual hosts. Should go _before_ the Match directives
    # because it might set up a different user name.
    Host PROJECT-whatever
        HostName whatever.example.com
        LocalForward 1234 localhost:1234

    # Hosts that should have agent forwarding are configured like this, based on
    # which agents to forward, and the UID of the remote user (which OpenSSH
    # doesn't know in advance, so it's a manual setup). Also, on some of the hosts
    # my SSH key will be in root's authorized_keys as well. But doing
    #     ssh root@PROJECT-whatever
    # with GnuPG socket forwarding active will let root own the remote end of the
    # forwarded socket, which breaks it for my normal user. Therefore these should
    # not be enabled when logging in as root, which is why there's an additional
    # `user` check.
    Match user scy originalhost PROJECT-whatever,PROJECT-another
        Include forward-ssh-agent.snip
        Include forward-gpg-1000.snip

    # A host that should have access to my SSH agent no matter whether I'm logging
    # in as root or not should look like this:
    Match user scy,root originalhost PROJECT-third
        Include forward-ssh-agent.snip
    ```
