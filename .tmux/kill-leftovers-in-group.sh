#!/bin/sh

group_name="${1:-main}"

for session in $( \
                 tmux list-sessions -F '#{session_attached} #{session_grouped} #{session_group} #{session_name}' \
                 | awk "/^0 1 $group_name / && \$4 != \"$group_name-base\" { print \$4 }" \
                ); do
	tmux kill-session -t "=$session"
done
