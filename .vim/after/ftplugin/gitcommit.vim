" The first line in Git commits should not be longer than 50 characters:
" <https://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html>
" Therefore, display an additional color column, but only while the cursor is
" in the first line.
function! s:ToggleFirstLine()
	if getcurpos()[1] == 1
		setlocal colorcolumn+=51
	else
		setlocal colorcolumn-=51
	endif
endfunction

autocmd CursorMoved,CursorMovedI <buffer> call s:ToggleFirstLine()

" And since we might be on line 1 right now, call it manually once.
silent call s:ToggleFirstLine()
