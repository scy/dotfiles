" When opening a help window, if the terminal is at least 160 columns wide,
" make it a vertical split instead of a horizontal one. Also, take care to not
" move windows that were already open. Additionally, try hard to not have the
" help window be _wider_ than 80 columns either.
" Based loosely on <https://stackoverflow.com/q/630884/417040>.

fun! s:LimitHelpWinSize()
	for s:win in getwininfo()
		let l:winnr = s:win['winnr']
		if getwinvar(l:winnr, '&buftype') == 'help' && winwidth(l:winnr) > 80
			exe s:win['winnr'] . 'windo silent vert res 80'
		endif
	endfor
endfun

autocmd! BufWinEnter <buffer>
	\ if !exists('w:moved')
	\ | let w:moved = 1
	\ | if &columns >= 160
		\ | wincmd L
		\ | endif
	\ | endif
	\ | setlocal winfixwidth
	\ | vert res 80 " Because LimitHelpWinSize won't find the window yet.

" Try to ensure max width.
autocmd! WinEnter,WinLeave <buffer> call s:LimitHelpWinSize()
autocmd! VimResized * call s:LimitHelpWinSize()
