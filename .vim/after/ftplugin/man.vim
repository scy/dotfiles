" Don't need to spellcheck other peoples' writing.
setlocal nospell

" Configure folding in man pages. I am aware that there is
" `g:ft_man_folding_enable`, but I'm not that happy with it. It uses
" foldmode=indent with an explicitly set shiftwidth of 8, and it only starts
" to fold code examples and stuff _inside_ of sections, because lines in man
" pages (at least the ones I'm looking at) start with 7 spaces, to have the
" text be aligned in the 8th column. Reducing the shiftwidth helps. See :h
" fold-indent for more explanation.
" Note that this does not _enable_ folding: I want to see the whole manpage
" when opening it, but a simple "zi" will allow me to fold everything down.
setlocal shiftwidth=4 foldmethod=indent foldnestmax=1 shiftwidth=4

" Some display default the Neovim plugin comes with, but the Vim one doesn't.
setlocal nolist

" The upstream ftplugin/man.vim sets foldcolumn=0, I don't agree with this.
if exists("*ScyFoldColumnAuto")
	call ScyFoldColumnAuto()
endif
