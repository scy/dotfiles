" Right click to mark.
nmap <buffer> <silent> <RightMouse> <LeftMouse>mf

" Double click to open.
nmap <buffer> <silent> <2-LeftMouse> <CR>

" Replacements for <CTRL-H> and <CTRL-L> that are shadowed by my window
" navigation mappings.
nnoremap <buffer> <silent> <LocalLeader><C-H> <Plug>NetrwHideEdit
nnoremap <buffer> <silent> <LocalLeader><C-L> <Plug>NetrwRefresh
" Principle of Least Surprise ;)
nnoremap <buffer> <silent> <F5>               <Plug>NetrwRefresh

" Some favorite directories.
nnoremap <buffer> <silent> . :exe ':e' getcwd()<CR>
nnoremap <buffer> <silent> ~ :e $HOME<CR>
nnoremap <buffer> <silent> p :e $HOME/proj<CR>

" When opening Netrw from a window that's currently editing a file, and Netrw is
" displaying the directory containing that file (as is common, e.g. when running
" :Explore), try to move the cursor to the line containing that file, to provide
" some continuity.
" However, Netrw has its own cursor-manipulating logic, to make sure that it
" stays on the same file name when changing sort order etc. I didn't find a way
" to hook into that, therefore I'm using a timer to do the jump, but only once
" per directory.
if exists('w:netrw_prvfile') && exists('b:netrw_curdir') && (!exists('b:jumptoprvfile_dir') || b:jumptoprvfile_dir != b:netrw_curdir)
	let b:jumptoprvfile_dir = b:netrw_curdir
	if b:netrw_curdir == fnamemodify(w:netrw_prvfile, ':h')
		" To find the current file name, look for the file name surrounded by
		" whitespace or start and end of line. This is more of a heuristic, but
		" improving it would require distinguishing between the display mode.
		" We search backwards to not end up on a file in an expanded
		" subdirectory in tree view.
		call timer_start(1, {-> search('\V\(\^\| \)' . escape(fnamemodify(w:netrw_prvfile, ':t'), '\') . '\(\$\| \)', 'bw')})
	endif
endif
