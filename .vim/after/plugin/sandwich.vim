if exists("g:sandwich#recipes")
	" Derive from default config.
	let g:sandwich#recipes = deepcopy(g:sandwich#default_recipes)

	let g:sandwich#recipes += [
		\ { 'buns': [ '»', '«' ] },
		\ { 'buns': [ '„', '“' ] },
		\ { 'buns': [ '“', '”' ] },
	\ ]
endif
