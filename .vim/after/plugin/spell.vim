" Note: Both Vim and Neovim are able to download spell files on demand.
" However, they depend on the bundled spellfile.vim plugin for this to work.
" Being a plugin, it is only available _after_ the vimrc has been loaded.
" Thus, setting 'spelllang' in vimrc usually does not work. Neovim catches
" that special edge case in its C source code:
" https://github.com/neovim/neovim/blob/1094d0c0dbd0f37ccc0f1d18c73c6066e5690664/src/nvim/spell.c#L1621-L1628
" Vim does not. Therefore, I'm setting the spelllang in an after/ plugin, to
" ensure that it works with both Vim and Neovim.

" Make sure that the directories for dictionaries exist. I used to have a .keep
" file in Git for them, but this doesn't work on machines where I have to
" symlink the directory into its location, because Git wouldn't accept (rightly
" so) a symlink instead of an actual directory.
call mkdir($HOME . '/.vim/spell/local', 'p')

" I am usually writing in US English or German.
set spelllang=en_us,de_de

" Enable spell checking.
set spell

" Limit suggestions to 10 items.
set spellsuggest=10

" Where to store my personal word lists for English and German.
set spellfile=~/.vim/spell/local/en.utf-8.add,~/.vim/spell/local/de.utf-8.add
