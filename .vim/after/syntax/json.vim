" Allow comments in JSON. Or, at least, don't highlight them IN GLARING RED.
" Yes, I am fully aware that JSON doesn't have comments, but that doesn't stop
" Microsoft for example from using JSON-with-comments for their config files.
" Inspired by <https://stackoverflow.com/a/68933130/417040>.
syntax clear jsonCommentError
syntax match jsonComment "//.*"
syntax region jsonComment start=/\/\*/ end=/\*\//
hi def link jsonComment Comment
