augroup HelpFTDetect
	au!
	au BufRead,BufNewFile ~/.vim/doc/*.txt,~/.vim/pack/*/doc/*.txt set filetype=help
augroup end
