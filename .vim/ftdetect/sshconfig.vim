" See .ssh/config; these are other locations that can have my private SSH
" config files.
au BufRead,BufNewFile */.ssh/private_config,*/ssh/config setfiletype sshconfig
