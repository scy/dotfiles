augroup SystemDFTDetect
	au!
	au BufRead,BufNewFile *.automount,*.device,*.mount,*.path,*.scope,*.service,*.slice,*.socket,*.swap,*.target,*.timer set filetype=systemd
augroup end
