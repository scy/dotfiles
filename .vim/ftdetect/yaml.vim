augroup YAMLFTDetect
	au!
	" Ansible Jinja2 templates.
	au BufRead,BufNewFile *.yml.j2 set ft=yaml
augroup end
