" Don't apply highlight-textwidth.vim in help files.
setlocal colorcolumn=

" I don't need scroll offsets in help files.
setlocal scrolloff=0 sidescrolloff=0

" Don't wrap lines, but do let me know if there's text to the left or right.
" However, 'listchars' is global in Vim (not Neovim), but I don't want to see
" my standard tab markers in help files. Hence, only do all of this in Neovim.
if has('nvim')
	setlocal nowrap list listchars=tab:\ \ ,extends:>,precedes:<
endif
