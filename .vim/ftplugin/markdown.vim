" Whitelist of languages that will be syntax-highlighted in fenced Markdown
" blocks. Can also be x=y mappings where y is the Vim filetype.
let g:markdown_fenced_languages = [
	\ 'bash=sh',
	\ 'html',
	\ 'javascript',
	\ 'js=javascript',
	\ 'json',
	\ 'python',
	\ 'sh',
	\ 'sshconfig',
	\ 'typescript',
	\ 'vim',
	\ 'yaml'
\ ]

" Toggle the "checked" state of a task. Expects `line` to be the string contents
" of the line. If it's not a list item already, it will be converted to one. If
" it doesn't contain a task checkbox already, it will receive one with an
" initial state of "unchecked".
fun! s:ToggleCheckbox(line)
	" Submatches:
	"   1: whitespace used for indentation
	"   2: list item marker: asterisk, dash, plus, or a number followed by . or )
	"   3: whitespace after list item marker
	"   4: checkbox and trailing whitespace
	"   5: space or x in the checkbox
	"   6: trailing whitespace after checkbox
	"   7: anything else in the line
	let m = matchlist(a:line, '\v^(\s*)([*+-]|[0-9]+[.)])(\s*)(\[( |x)\](\s+))?(.*)$')
	if empty(m)  " Not a list item at all.
		" Make it a list item by introducing an item marker and empty checkbox.
		return substitute(a:line, '\v^(\s*)(.*)', '\1- [ ] \2', '')
	elseif empty(m[4])  " List, but not a task.
		" Insert an empty checkbox between list marker and text.
		return m[1] . m[2] . ' [ ]' . m[3] . m[7]
	elseif m[5] == 'x'  " Checked-off list item.
		" Uncheck the item.
		return m[1] . m[2] . m[3] . '[ ]' . m[6] . m[7]
	else  " Not-checked-off list item.
		" Check off the item.
		return m[1] . m[2] . m[3] . '[x]' . m[6] . m[7]
	endif
endfun
" Toggle the checkbox state for a line. Expects `line` to be a line number
" compatible with line().
fun! MarkdownToggleCheckbox(line)
	let linenum = line(a:line)
	let line = getline(linenum)
	let replacement = s:ToggleCheckbox(line)
	call setline(linenum, replacement)
	if linenum == line('.') && len(replacement) > len(line)
		" Move the cursor accordingly.
		let pos = getpos('.')
		let pos[2] += len(replacement) - len(line)
		call setpos('.', pos)
	endif
endfun
" Remove the checkbox for a line. Expects `line` to be a line number compatible
" with line().
fun! MarkdownRemoveCheckbox(line)
	let linenum = line(a:line)
	let line = getline(linenum)
	let replacement = substitute(line, '\v(\s*([*+-]|[0-9]+[.)])\s*)(\[[ x]\]\s*)', '\1', '')
	let pos = getpos('.')
	call setline(linenum, replacement)
	if linenum == line('.') && len(replacement) < len(line)
		" Move the cursor accordingly.
		let pos[2] = max([1, pos[2] - (len(line) - len(replacement))])
		call setpos('.', pos)
	endif
endfun

" <LocalLeader>x will toggle the checkbox state or introduce one.
nnoremap <silent><buffer> <LocalLeader>x :call MarkdownToggleCheckbox('.')<CR>
" <LocalLeader>X will remove the checkbox if there is one.
nnoremap <silent><buffer> <LocalLeader>X :call MarkdownRemoveCheckbox('.')<CR>
" <LocalLeader>dx will delete all lines with a checked-off checkbox.
nnoremap <silent><buffer> <LocalLeader>dx :g/\v\s*([*+-]<Bar>[0-9]+[.)])\s+\[x\]\s+.*$/d _<CR>
" In visual mode, only delete completed lines within the selection.
vnoremap <silent><buffer> <LocalLeader>dx :g/\v%V\s*([*+-]<Bar>[0-9]+[.)])\s+\[x\]\s+.*$%V/d _<CR>
