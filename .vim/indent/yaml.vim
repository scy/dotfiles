" Disable Vim's builtin indent script for YAML, it's trying to be way smarter
" than it actually is in my experience.
let b:did_indent = 1
