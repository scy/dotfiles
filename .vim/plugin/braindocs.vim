let s:braindocs_dir = $HOME . "/proj/braindocs"
let s:daily_notes_dir = s:braindocs_dir . "/knowledge/blog/posts"

function! s:system(cmd)
	let res = system(a:cmd)
	if v:shell_error != 0
		throw (type(a:cmd) == v:t_list ? join(a:cmd, " ") : a:cmd) . " returned " . v:shell_error . ": " . res
	endif
	return res
endfunction

function! s:edit_date(tpl, date, bang)
	if match(a:date, '\v^-\d+$') != -1
		let date = 'today ' . a:date . ' days'
	else
		let date = a:date
	endif
	try
		let fname = s:system(["date", "-d", date, "+" . a:tpl])
	catch
		echoerr v:exception
		return
	endtry
	execute "edit" . (a:bang ? "!" : "") fname
endfunction

function! s:new_daily_note(fname)
	let match = matchlist(a:fname, '\v/(\d{4})/(\d{2})/(\d{2})\.md')
	if empty(match)
		return
	endif
	execute "1Tpl! dn" match[1] . "-" . match[2] . "-" . match[3]
endfunction

" :DN! [date]
" Start editing my "daily notes" file for [date] (default: today). If the
" current buffer has unsaved changes, this command will refuse to abandon them
" unless the optional ! is given.
command! -bang -bar -nargs=? DN call s:edit_date(s:daily_notes_dir . "/%Y/%m/%d.md", empty(<q-args>) ? "now" : <q-args>, <bang>0)

augroup Braindocs
	autocmd!
	" When creating a new daily note, insert a template for that day.
	execute "autocmd BufNewFile" s:daily_notes_dir . "/*.md call s:new_daily_note(expand('<afile>'))"
augroup END
