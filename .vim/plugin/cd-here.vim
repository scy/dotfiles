" :CD
" Change (global) current working directory to that of the current file.
command! -bar -nargs=0 CD execute 'cd' expand('%:p:h')

" :LCD
" Change the buffer's current working directory to that of the current file.
command! -bar -nargs=0 LCD execute 'lcd' expand('%:p:h')

" :TCD
" Change the tab's current working directory to that of the current file.
command! -bar -nargs=0 TCD execute 'tcd' expand('%:p:h')
