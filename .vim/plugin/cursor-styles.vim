" Changing cursors on mode change. (tmux also uses TERM=screen, sometimes.)
if !has("nvim")  " Neovim doesn't use these, see `:h t_xx` in its help.
	if &term =~ "xterm" || &term =~ "screen" || &term =~ "tmux"
		let &t_SI = "\<Esc>[5 q" " start insert: blinking bar
		let &t_SR = "\<Esc>[3 q" " start replace: blinking underscore
		let &t_EI = "\<Esc>[1 q" " end insert/replace: blinking block
	endif
endif
