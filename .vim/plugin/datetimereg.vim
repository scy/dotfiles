" Mappings to insert the current date or time in various formats using a
" <C-R><Leader> prefix.
let s:mappings = {
	\ 'd': '%Y-%m-%d',
	\ 'D': '%a %d %B %Y',
	\ 't': '%H:%M:%S',
\ }

for [k, v] in items(s:mappings)
	exe 'inoremap <silent> <C-R><Leader>' . k '<C-R>=strftime(''' . v . ''')<CR>'
	exe 'cnoremap <C-R><Leader>' . k '<C-R>=strftime(''' . v . ''')<CR>'
endfor
