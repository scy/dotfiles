" Hitting <CR> in normal mode inserts an empty line (no comment characters or
" indentation either) before the current one.

fun! ScyEnter()
	" If we're on a closed fold, open it.
	if foldclosed('.') != -1
		normal! zo
		return
	endif

	" If we're in a help window, follow the link under the cursor.
	if &buftype == 'help'
		execute "normal! \<C-]>"
		return
	endif

	" If we're in a read-only buffer, or if we're in any kind of special
	" buffer, do nothing and act like we weren't bound at all. The latter is
	" important for things like quickfix windows, Neovim's "gO" lists, etc.
	if &readonly || &buftype != ''
		execute "normal! \<CR>"
		return
	endif

	call append(line('.') - 1, '')
endfun

if has('nvim')
	nnoremap <silent> <CR> <Cmd>call ScyEnter()<CR>
else
	nnoremap <silent> <CR> :call ScyEnter()<CR>
endif
