" I usually don't open help on its start page, but always with a search term.
" Therefore, using <F1> to open help is useless to me. Instead, make it
" _close_ the help window, because :helpc is too long to type.
if has('nvim')
	nnoremap <silent> <F1> <Cmd>helpc<CR>
	inoremap <silent> <F1> <Cmd>helpc<CR>
else
	nnoremap <silent> <F1> :helpc<CR>
	inoremap <silent> <F1> <C-O>:helpc<CR>
endif
