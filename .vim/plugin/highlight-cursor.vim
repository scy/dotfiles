" Pressing <Leader> twice will highlight the current cursor position.
function! ScyCursorCross()
	let s:old_line = &cursorline
	let s:old_col = &cursorcolumn
	setlocal cursorline cursorcolumn
	call timer_start(200, {-> execute(['let &l:cursorline = ' . s:old_line, 'let &l:cursorcolumn = ' . s:old_col])})
endfunction

if has('nvim')
	nnoremap <silent> <Leader><Leader> <Cmd>call ScyCursorCross()<CR>
else
	nnoremap <silent> <Leader><Leader> :call ScyCursorCross()<CR>
endif
