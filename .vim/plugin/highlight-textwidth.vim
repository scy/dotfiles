" Mark the column to the right of 'textwidth' in an unobtrusive manner (dark
" gray on 16-color terminals, very dark gray on 256-color ones).
set colorcolumn=+1
highlight ColorColumn ctermbg=darkgray
if &t_Co >= 256
	highlight ColorColumn ctermbg=236
endif
