" Also apply EditorConfig to new empty buffers.
let g:EditorConfig_enable_for_new_buf = 1

" No need to have the EditorConfig plugin show a line length indicator, I
" already set colorcolumn=+1.
let g:EditorConfig_max_line_indicator = 'none'

" Play nice with Fugitive, as suggested by the EditorConfig plugin's readme.
let g:EditorConfig_exclude_patterns = [
	\ 'fugitive://.*',
\ ]

" Disable EditorConfig plugin for certain filetypes.
augroup CustomEditorConfig
	autocmd!
	autocmd FileType diff,gitcommit let b:EditorConfig_disable = 1
augroup END

" Neovim comes with EditorConfig support built-in starting in 0.9. They use
" their own Lua implementation. Vim bundles the official editorconfig-vim
" plugin since 9.0.1799, but (at the time of writing, 2024-02-17) it is
" lagging behind the submodule I'm including in my dotfiles repo. Therefore,
" load the pack in the submodule, except if we're on Neovim ≥ 0.9.
if !has('nvim-0.9')
	packadd editorconfig-vim
endif
