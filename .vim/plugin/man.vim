if !has('nvim')
	" In Vim (not Neovim), the man plugin needs to be loaded man-ually.
	runtime ftplugin/man.vim
endif
