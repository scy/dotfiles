let g:miaufmt_formatters = {
	\ 'prettier': {
		\ 'args': [ '--stdin-filepath', '%' ],
	\ },
	\ 'ruff': {
		\ 'args': [ 'format', '--stdin-filename', '%', '-' ],
	\ },
\ }


let g:miaufmt_filetypes = {
	\ 'python': {
		\ 'formatters': [ 'ruff' ],
	\ },
	\ 'typescript': {
		\ 'formatters': [ 'prettier' ],
	\ },
\ }


command! -bar Miaufmt call Miaufmt()


function! Miaufmt_get_formatter(filetype)
	let l:ftconfig = get(g:miaufmt_filetypes, a:filetype, {})
	for l:formname in get(l:ftconfig, 'formatters', [])
		let l:formdef = get(g:miaufmt_formatters, l:formname, v:null)
		if type(l:formdef) != type({})
			continue
		endif
		let l:formdef = extend({
			\ 'cmd': l:formname,
			\ 'args': [],
		\ }, l:formdef)
		if !executable(l:formdef['cmd'])
			continue
		endif
		return l:formdef
	endfor
	return v:null
endfunction


function! Miaufmt()
	let l:formatter = Miaufmt_get_formatter(&filetype)
	if l:formatter is v:null
		return
	endif
	let l:view = winsaveview()
	" Format the file.
	let l:filtercmd = printf('%%!%s %s', shellescape(l:formatter['cmd']), join(map(copy(l:formatter['args']), 'shellescape(expandcmd(v:val), 1)')))
	silent execute 'keepmarks keepjumps ' . l:filtercmd
	" If the filter failed, undo the change.
	if v:shell_error
		echoerr printf('miaufmt: formatter exited with %d: %s', v:shell_error, l:filtercmd)
		undo
	endif
	call winrestview(l:view)
endfunction


augroup Miaufmt
	au!
	au BufWritePre * Miaufmt
augroup END
