" Combination of ZyX's <https://stackoverflow.com/a/4294176/417040> and Tom
" Hale's <https://stackoverflow.com/a/42872275/417040>.
function s:MkNonExDir(file, buf)
	if empty(getbufvar(a:buf, '&buftype')) && a:file !~# '\v^\w+\:\/'
		let dir = fnamemodify(a:file, ':h')
		if !isdirectory(dir)
			if input("'" . dir . "' does not exist. Create it? [y/N] ") =~? '^y\%[es]$'
				call mkdir(dir, 'p')
			endif
		endif
	endif
endfunction
augroup MkDirOnSave
	autocmd!
	autocmd BufWritePre * :call s:MkNonExDir(expand('<afile>'), +expand('<abuf>'))
augroup END
