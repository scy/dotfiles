" This is a work-in-progress plugin I use for managing Markdown notes.


function! notizen#BaseDir()
	return fnamemodify("~/proj/notes", ":p")
endfunction


function! notizen#InsertFrontMatter()
	let l:yaml = [
		\ "---",
		\ "title: ",
		\ "tags: []",
		\ "---",
		\ "",
	\ ]
	execute "normal! ggi\<C-R>=l:yaml\<CR>\<Esc>"
endfunction


function! notizen#NewIDPath()
	let l:now = localtime()
	return strftime("%Y/%m/", l:now) . printf("%09x", now)
endfunction


function! notizen#NewPath(...)
	let l:slug = get(a:, "1", "")
	if l:slug != ""
		let l:slug = "-" . l:slug
	endif
	return notizen#BaseDir() . notizen#NewIDPath() . l:slug . notizen#Suffix()
endfunction


function! notizen#Suffix()
	return ".md"
endfunction


function! notizen#EditNew(...)
	execute "e " . notizen#NewPath(get(a:, "1", ""))
	call notizen#InsertFrontMatter()
	normal! 2gg
	startinsert!
endfunction


function! notizen#Foo(...)
	echo a:
endfunction


command! -nargs=? NotizenNew call notizen#EditNew(<f-args>)
