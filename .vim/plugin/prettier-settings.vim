" Only run Prettier if it can find a config file.
let g:prettier#autoformat_config_present = 1

" Format files even without the @format pragma.
let g:prettier#autoformat_require_pragma = 0

" .prettierrc settings overrule vim-prettier's questionable defaults.
let g:prettier#config#config_precedence = "prefer-file"
