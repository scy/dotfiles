" Auto-indent new lines based on the one before.
set autoindent

" Preserve the indentation style (tabs/spaces) of the other line.
set copyindent preserveindent

" Default text width is 80 characters.
set textwidth=80

" Default formatting:
"   - c: wrap comments at 'textwidth'
"   - j: remove comment leader when joining lines
"   - n: recognize (numbered) lists when formatting (see 'formatlistpat')
"   - o: automatically add comment leader after `o` or `O`
"   - q: allow formatting comments with `gq`
"   - r: automatically add comment leader after <Enter>
"   - /: only add `//` comment leaders when the previous `//` is at the start
"        of the line
" Optional formatting:
"   - a: automatically format comment paragraphs while editing them
set formatoptions=cjnoqr/

function! s:ToggleFormatOption(option, name)
	execute 'setlocal formatoptions' . (&formatoptions =~# a:option ? '-' : '+') . '=' . a:option
	echo a:name 'is now' (&formatoptions =~# a:option ? 'enabled' : 'disabled')
endfunction
nnoremap <silent> <Leader>fa :call <SID>ToggleFormatOption('a', 'format while typing')<CR>
