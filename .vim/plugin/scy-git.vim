" Settings for Git and related plugins.

" Custom mappings for GitGutter.
let g:gitgutter_map_keys = 0
nnoremap <Leader>ga <Plug>(GitGutterStageHunk)
nnoremap <Leader>gp <Plug>(GitGutterPreviewHunk)
nnoremap <Leader>gu <Plug>(GitGutterUndoHunk)
" These are the defaults, but I need to map them manually since I've asked
" GitGutter to not map anything.
nnoremap [c <Plug>(GitGutterPrevHunk)
nnoremap ]c <Plug>(GitGutterNextHunk)
onoremap ic <Plug>(GitGutterTextObjectInnerPending)
onoremap ac <Plug>(GitGutterTextObjectOuterPending)
xnoremap ic <Plug>(GitGutterTextObjectInnerVisual)
xnoremap ac <Plug>(GitGutterTextObjectOuterVisual)

" Close GitGutter preview windows when pressing escape.
let g:gitgutter_close_preview_on_escape = 1

" Allow some of my aliases (e.g. "gca" in the shell) to be invoked as Vim
" commands (e.g. ":Gca").
for cmd in ['c', 'ca', 'cam', 'caam', 'd', 'ds', 'l', 'lp', 'ls', 'p', 's']
	execute 'command! -bar -nargs=* G' . cmd 'G' cmd '<args>'
endfor

" :Ga [files]
" Adds the given [files] to the index. If [files] is empty, adds the current
" file. FIXME: When specifying file names, completion is relative to Vim's CWD,
" but the Git command will run in the repo root.
command! -bar -complete=file -nargs=* Ga execute 'G a' empty(<q-args>) ? expand('%:p') : <q-args>
