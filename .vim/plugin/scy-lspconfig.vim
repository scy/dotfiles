if has('nvim')
lua << END
local on_attach = function(client)
  local highlight = function()
    vim.lsp.buf.clear_references()
    vim.lsp.buf.document_highlight()
  end

  -- Run the first function if Telescope is enabled, else the second one.
  local if_telescope_else = function(telescope, no_telescope)
    return function()
      if vim.g.scy_telescope_enabled then
        return require("telescope.builtin")[telescope]()
      else
        return no_telescope()
      end
    end
  end

  local caps = client.server_capabilities
  local buf = vim.api.nvim_get_current_buf()
  local opts = {buffer=buf}

  if caps.codeActionProvider then
    vim.keymap.set('n', '<C-Space>', vim.lsp.buf.code_action, opts)
  end

  if caps.completionProvider then
    -- Enable completion triggered by <c-x><c-o>
    vim.bo[buf].omnifunc = 'v:lua.vim.lsp.omnifunc'
  end

  if caps.definitionProvider then
    vim.keymap.set('n', 'gd', if_telescope_else("lsp_definitions", vim.lsp.buf.definition), opts)
  end

  if caps.documentFormattingProvider then
    vim.keymap.set('n', '<LocalLeader>ff', function()
      vim.lsp.buf.formatting({async=true})
    end, opts)
  end

  -- Highlight references to the symbol under the cursor, every time the cursor
  -- moves. Might disable this again if it's too slow, and change to CursorHold
  -- & CursorHoldI instead (but keep the clear_references() on CursorMoved).
  if caps.documentHighlightProvider then
    vim.api.nvim_create_autocmd({"CursorMoved", "CursorMovedI"}, {
      buffer = buf,
      callback = highlight,
    })
  end

  if caps.hoverProvider then
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
  end

  if caps.referencesProvider then
    vim.keymap.set('n', 'gr', if_telescope_else("lsp_references", vim.lsp.buf.references), opts)
  end

  if caps.renameProvider then
    vim.keymap.set('n', 'cn', vim.lsp.buf.rename, opts)
  end

  if caps.signatureHelpProvider then
    vim.keymap.set('i', '<C-Space>', vim.lsp.buf.signature_help, opts)
  end

end

local lspconfig = require('lspconfig')

lspconfig.pylsp.setup{on_attach=on_attach}

-- ruff-lsp is deprecated, ruff server is the replacement.
lspconfig.ruff_lsp.setup{cmd={'ruff', 'server'}, on_attach=on_attach}

lspconfig.tsserver.setup{}

-- Global mappings.
vim.keymap.set('n', '<LocalLeader>e', vim.diagnostic.open_float)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<LocalLeader>q', vim.diagnostic.setloclist)

END
endif
