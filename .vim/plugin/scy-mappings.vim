" Custom mappings for plugins and also my own little hacks.

" This is supposed to become the file where all of my custom mappings are
" defined, instead of defining them directly in (my own) plugin files. However,
" I have not yet migrated all of them.

" vim-sandwich breaks the is/as and ib/ab text objects (sentence and "block") by
" default, see <https://github.com/machakann/vim-sandwich/issues/62>. I am
" keeping the normal mode mappings, but replace the default text object mappings
" with S ("sandwich") and Q ("sandwich query").
let g:textobj_sandwich_no_default_key_mappings = 1
omap iS <Plug>(textobj-sandwich-auto-i)
xmap iS <Plug>(textobj-sandwich-auto-i)
omap aS <Plug>(textobj-sandwich-auto-a)
xmap aS <Plug>(textobj-sandwich-auto-a)
omap iQ <Plug>(textobj-sandwich-query-i)
xmap iQ <Plug>(textobj-sandwich-query-i)
omap aQ <Plug>(textobj-sandwich-query-a)
xmap aQ <Plug>(textobj-sandwich-query-a)
