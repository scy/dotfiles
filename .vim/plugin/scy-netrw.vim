" Disable the banner. Can be re-enabled with `I`.
let g:netrw_banner = 0

" Use human-readable file sizes, base 1024.
let g:netrw_sizestyle = 'H'

" Use "Thu 09 May 2024 16:29:16 CEST" as the date/time format. Note that I'm
" adding a tab at the beginning, to get consistent spacing to the file size. See
" g:netrw_bufsettings below.
let g:netrw_timefmt = '	%a %d %b %Y %H:%M:%S %Z'

" Default settings for netrw buffers. The ones at the start are the defaults
" from the manual, but `ts=5 nolist` moves file size and modification date
" apart. Sadly, netrw doesn't align the file size by default, causing the date
" to be all over the place. But, when using g:netrw_sizestyle='H', the width of
" the size field is rather constant, and by adding a tab in the timefmt string
" and choosing a good ts value we can get some sort of okayish alignment.
let g:netrw_bufsettings = 'noma nomod nonu nowrap ro nobl ts=5 nolist'

" Netrw's default mouse mappings are awful (single click to open, right click to
" delete), which is why I'm disabling them and define my own ones in
" after/ftplugin/netrw.vim.
let g:netrw_mousemaps = 0

" By default, use xdg-open for "open in external viewer". These usually don't
" support remote URLs.
let g:netrw_browsex_viewer = 'xdg-open'
let g:netrw_browsex_support_remote = 0

" Show files as a tree by default.
let g:netrw_liststyle = 3

" Default size of the split explorer is 20%.
let g:netrw_winsize = 20

" Explore directory of current file.
nnoremap <silent> <Leader>ef :Explore<CR>
nnoremap <silent> - :Explore<CR>

" Toggle file tree.
nnoremap <silent> <Leader>el :Lexplore<CR>

" Return to explorer.
nnoremap <silent> <Leader>er :Rexplore<CR>
