" A function to shorten the current encoding as much as possible.
function! ShortFileEncoding()
	" If no file encoding is set, use the system encoding.
	let e = (&fileencoding == "") ? &enc : &fenc
	if e[0:3] == "utf-"
		" Just return the number for UTF encodings.
		let r = e[4:]
	elseif e == "latin1"
		" That's actually ISO-8859-1.
		let r = "-1"
	elseif e[0:8] == "iso-8859-"
		" Return a dash and the number for ISO-8859 encodings.
		let r = e[8:]
	elseif e[0:1] == "cp"
		" Just return the number for Windows code pages (at least 3 digits)
		let r = e[2:]
	else
		" Okay, no short version. Return the full name.
		let r = e
	endif
	" Add a question mark if fenc is not set.
	if &fileencoding == ""
		let r .= "?"
	endif
	return r
endfunction

" A compact status line packed with information.
" This is basically what I had back in 2010:
" <https://github.com/scy/dotscy/commit/bd83019c7e8ffebec08bc87d171bd18b04b06b1d>
" Uses two spaces before the &modified bullet point but will print just one, see
" <https://github.com/neovim/neovim/issues/28918> and
" <https://github.com/vim/vim/issues/3898>.
let &statusline = '%{fnamemodify(expand(''%%:p''), '':~:.'')}'
	\ . '%5r'
	\ . '%{&modified ? "  •" : ""}'
	\ . '%='
	\ . '%{&ft == "" ? "?" : &ft}'
	\ . ' %{toupper(&ff[0:0])}'
	\ . '%{ShortFileEncoding()}%{&bomb?"!":""}'
	\ . ' %{&et ? ")".&ts."(" : &ts}'
	\ . ' U+%04B'
	\ . ' %l:%c%V'
	\ . ' %P/%LL'
