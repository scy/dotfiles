" Nicer colors.
hi FoldColumn ctermfg=137 ctermbg=NONE
hi Folded     ctermfg=137 ctermbg=NONE

" Don't necessarily fill up the fold to the right of the screen.
set fillchars+=fold:\ ,

" Use fancier characters in the foldcolumn.
if has('nvim-0.5.0') || has('patch-8.2.2524')
	set fillchars+=foldopen:⏷,foldclose:⏵,foldsep:⁝
endif

" Set 'foldcolumn' and 'signcolumn' automatically based on whether folding is
" enabled or not. If there is a foldcolumn, always enable the signcolumn too,
" else the foldcolumn is so crammed to the text. If there is no foldcolumn,
" set the signcolumn to auto.
" Neovim can do foldcolumn=auto, Vim cannot. Also, Neovim can do multiple
" signcolumns and define a minimum and maximum, Vim cannot.
if has('nvim')
	let s:fcol_width = 'auto:5'
	let s:scol_width = 'auto:1-9'
else
	let s:fcol_width = '3'
	let s:scol_width = 'yes'
endif
fun! ScyFoldColumnAuto()
	let &foldcolumn = &foldenable ? s:fcol_width : '0'
	let &signcolumn = &foldenable ? s:scol_width : 'auto'
endfun

" Text to show in a closed fold. Basically, just the first line and a length.
" This is inspired by Greg Sexton's version:
" <https://web.archive.org/web/20161017143651/http://www.gregsexton.org:80/2011/03/improving-the-text-displayed-in-a-fold/>
fun! ScyFoldText()
	" Find first non-blank line.
	let nonblank = v:foldstart
	while nonblank <= v:foldend && getline(nonblank) =~ '^\s*$'
		let nonblank = nextnonblank(nonblank + 1)
	endwhile
	if nonblank > v:foldend
		let nonblank = v:foldstart
	endif

	" Get the line, replace tabs with the correct amount of spaces.
	let line = substitute(getline(nonblank), '\t', repeat(' ', &tabstop), 'g')

	let length = 1 + v:foldend - v:foldstart
	let percent = length * 100.0 / line('$')
	return printf('%s  ⏷  %i line%s (%.1f%%)', line, length, length == 1 ? '' : 's', percent)
endfun
set foldtext=ScyFoldText()

" I was hoping to use "au OptionSet" here, but apparently there's no OptionSet
" triggered for 'foldenable' on zi, so I have to do change what zi does, and
" _then_ react to it. See <https://github.com/neovim/neovim/issues/25842>.
nnoremap <silent> zi :set foldenable!<CR>
augroup ScyStyleFold
	au!
	au OptionSet      foldenable,foldmethod call ScyFoldColumnAuto()
	au BufNew,BufRead *                     call ScyFoldColumnAuto()
augroup end
