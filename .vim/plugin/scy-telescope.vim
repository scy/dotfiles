" When using a non-supported version, Telescope will throw an error instead of
" simply disabling itself. Therefore, we only load it when a compatible version
" is being used. It's installed in packs/telescope/opt/telescope.nvim, see
" :h pack-add
let g:scy_telescope_enabled = 0
if has("nvim-0.9.0")
	let g:scy_telescope_enabled = 1
	packadd telescope.nvim

	" Mappings to invoke Telescope.
	nnoremap <Leader><Leader> <cmd>Telescope resume<cr>
	nnoremap <Leader>. <cmd>Telescope find_files<cr>
	nnoremap <Leader>fh <cmd>Telescope find_files cwd=~<cr>
	nnoremap <Leader>fv <cmd>Telescope find_files cwd=~/.vim<cr>
	nnoremap <Leader>fr <cmd>Telescope oldfiles<cr>
	nnoremap <Leader>b <cmd>Telescope buffers<cr>
	nnoremap <Leader>j <cmd>Telescope jumplist<cr>
	nnoremap <Leader>' <cmd>Telescope marks<cr>
	nnoremap <Leader>" <cmd>Telescope registers<cr>
	nnoremap <Leader>/ <cmd>Telescope live_grep<cr>
	nnoremap <LocalLeader>? <cmd>Telescope current_buffer_fuzzy_find<cr>
	nnoremap <Leader>gb <cmd>Telescope git_branches<cr>
	nnoremap <Leader>gc <cmd>Telescope git_commits<cr>
	nnoremap <Leader>fd <cmd>Telescope diagnostics bufnr=0<cr>
	nnoremap <Leader>fD <cmd>Telescope diagnostics<cr>
	nnoremap z= <cmd>Telescope spell_suggest<cr>
	nnoremap <Leader>f<Leader> <cmd>Telescope builtin<cr>

lua <<
local actions = require("telescope.actions")
require("telescope").setup{
	defaults = {
		mappings = {
			i = {
				-- close Telescope when pressing <Esc> once
				["<esc>"] = actions.close,
			},
		},
	},
}
.
endif
