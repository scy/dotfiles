" Set some features based on the terminal in use.
let g:has_real_bold = 0  " real bold text instead of making foreground bright
let g:has_undercurl = 0  " curly underlines
let g:has_256colors = &t_Co == 256  " supports displaying the 256 color palette
let g:has_16bgs = g:has_256colors   " supports 16 background colors, not just 8
if $TERM == 'xterm-kitty'
	set termguicolors
	let g:has_undercurl = 1
	let g:has_real_bold = 1
	" kitty will cut off italics at cell borders, closed as "won't fix".
	" <https://github.com/kovidgoyal/kitty/issues/4635>
	let g:srcery_italic = 0
endif
if $TERM == 'linux'
	" Yes, the Linux framebuffer can _interpret_ 256 colors, which is why &t_Co
	" is set to 256 there, but it will only map them to its 16-color palette.
	let g:has_256colors = 0
	" It also only supports 8 background colors, not the bright variants.
	let g:has_16bgs = 0
endif

" Always try to keep 5 lines and 15 columns visible around the cursor.
set scrolloff=5 sidescrolloff=15

" Show significant whitespace.
if &encoding == 'utf-8'
	set listchars=tab:›·,nbsp:¯,trail:·
else
	set listchars=tab:\|.,nbsp:_,trail:_
endif
set list

" Always show a status line, even if just one window is visible.
" I have thought about using Neovim's "global status line" feature
" (laststatus=3), but I find it too difficult to then determine the current
" window. Maybe there'll be some feature to help with that in the future.
set laststatus=2 noruler

" Soft-wrap long lines, but not in the middle of words.
set wrap linebreak

" Default tab width.
set tabstop=4 shiftwidth=4

" Update faster on CursorHold. (Does that really belong in the "visuals" file?)
set updatetime=500

" Neovim's line number redrawing while scrolling (with relative numbers
" enabled) is way faster than Vim's for some reason, which is why I enable
" relative numbers in Neovim, but not in Vim.
set number
let &relativenumber = has('nvim')

" My colorscheme is Srcery <https://srcery.sh/>.
set background=dark
colorscheme srcery

" Some fine-tuning of the colors coming up.
" Note that while Vim provides color names, they will map to different actual
" colors depending on whether your terminal supports 16 or 256 colors. So, while
" LightGreen equals 10 in 16-color mode, it's 121 in 256-color mode. Since the
" colors between 16 and 255 are usually not customizable but fixed, predefined
" colors, using these color names will likely make Vim choose colors that don't
" match the 16 colors your terminal emulator's color scheme defines. It's
" probably best to avoid the named colors altogether.

" Neovim does strange things with `ctermbg=242` instead of simply reversing
" video, and it breaks visual highlighting at least on my Linux framebuffer
" tmux. Therefore I'm explicitly configuring something that works reliably.
highlight Visual term=reverse cterm=reverse gui=reverse

" Use actual box-drawing characters in the vertical delimiter, and no
" background color.
set fillchars+=vert:│
hi VertSplit term=NONE cterm=NONE ctermfg=7 ctermbg=NONE guifg=#6c6c6c guibg=NONE
" The VertSplit should have the same color as the inactive status bar, which is
" 7 (light gray) by default, but 8 (dark gray) if the terminal supports 16-color
" backgrounds.
if g:has_16bgs
	highlight VertSplit ctermfg=8
endif

" Make line numbers unobtrusive, using the same color.
highlight LineNr ctermfg=8 guifg=#6c6c6c

" Highlight references to the same value (via LSP).
highlight LspReferenceRead  ctermbg=23 guibg=#005f5f
highlight LspReferenceWrite ctermbg=53 guibg=#5f005f

" Changing Srcery's default status line because I don't like it. Also taking
" care to make it look good no matter what the terminal's capabilities are.
execute 'highlight StatusLine   term=reverse cterm=NONE ctermbg=10 ctermfg=0 gui=bold guifg=' . g:srcery_black .        ' guibg=' . g:srcery_bright_green
execute 'highlight StatusLineNC term=reverse cterm=NONE ctermbg=7  ctermfg=0 gui=NONE guifg=' . g:srcery_bright_white . ' guibg=#6c6c6c'
if g:has_real_bold
	highlight StatusLine term=reverse,bold cterm=bold
endif
if g:has_16bgs
	highlight StatusLineNC ctermbg=8 ctermfg=15
endif

" I'm having a bit of a hard time seeing significant whitespace with Srcery's
" default settings.
highlight link Whitespace SrceryBlue

" In case the terminal doesn't support undercurls, set background colors, but
" keep them from burning your eyes out.
execute 'highlight SpellBad   cterm=NONE gui=NONE ctermfg=NONE guifg=NONE ctermbg=88 guibg=#870000 guisp=' . g:srcery_bright_red
execute 'highlight SpellCap   cterm=NONE gui=NONE ctermfg=NONE guifg=NONE ctermbg=20 guibg=#0000d7 guisp=' . g:srcery_blue
execute 'highlight SpellLocal cterm=NONE gui=NONE ctermfg=NONE guifg=NONE ctermbg=58 guibg=#5f5f00 guisp=' . g:srcery_bright_yellow
execute 'highlight SpellRare  cterm=NONE gui=NONE ctermfg=NONE guifg=NONE ctermbg=55 guibg=#5f00af guisp=' . g:srcery_bright_magenta

" However, if the terminal supports them (detected above), then use them! (And
" unset the background color.)
if g:has_undercurl
	highlight SpellBad   cterm=undercurl gui=undercurl ctermbg=NONE guibg=NONE
	highlight SpellCap   cterm=undercurl gui=undercurl ctermbg=NONE guibg=NONE
	highlight SpellLocal cterm=undercurl gui=undercurl ctermbg=NONE guibg=NONE
	highlight SpellRare  cterm=undercurl gui=undercurl ctermbg=NONE guibg=NONE
endif
