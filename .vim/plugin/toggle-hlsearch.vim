" :noh is too cumbersome to type. Also, this mapping allows us to turn the
" search highlighting back _on_ again, without moving!
nnoremap <silent> <Leader>h :let v:hlsearch = !v:hlsearch<CR>
