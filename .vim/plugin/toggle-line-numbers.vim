" A function to toggle line numbers on and off that's compatible to any
" setting of 'relativenumber'. That's because if 'relativenumber' is set, but
" 'number' is unset, Vim will still display line numbers, the only difference
" being that the line with the cursor is labeled `0` instead of the line
" number. Since I want to be able to toggle line numbers completely on and off
" without affecting -- or being affected by! -- 'relativenumber', I need to
" jump through some hoops.
" See `:h number_relativenumber` for the possible combinations.
let s:prev_rnu = &relativenumber
function! ToggleLineNumbers()
	if &number
		" Disable line numbers, remember the previous value of rnu.
		let s:prev_rnu = &relativenumber
		setlocal nonumber norelativenumber
	else
		let &relativenumber = s:prev_rnu
		setlocal number
	endif
endfunction

if has('nvim')
	noremap <silent> <Leader>n <Cmd>call ToggleLineNumbers()<CR>
	noremap <silent> <Leader>N <Cmd>set relativenumber!<CR>
else
	noremap <silent> <Leader>n :call ToggleLineNumbers()<CR>
	noremap <silent> <Leader>N :set relativenumber!<CR>
endif
