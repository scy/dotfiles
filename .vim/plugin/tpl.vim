function! s:tpl(line, args, edit)
	" If the line is 0, set it to the cursor's line.
	let line = a:line ? a:line : line(".")
	" Execute `tpl` and place the result above the given line.
	execute (line - 1) . "r!tpl" join(a:args)
	" Search backwards for an "XXX" marker and move the cursor there.
	if search("XXX", "bcwz") && a:edit
		" If the search was successful and a:edit is true, basically do "c3l".
		normal! 3x
		if col(".") == col("$")-1
			" Cursor at end of line, simple "startinsert" would be 1 to the
			" left of where we wanna be.
			startinsert!
		else
			startinsert
		endif
	endif
endfunction

" :[count]Tpl[!] name [args]
" Call `tpl` to display template `name`, supplying additional [args] to it if
" any are given. Insert the resulting text above line [count]. By default,
" [count] is 0 (the cursor's line). Jump to the `XXX` marker in the template
" (searching backwards for it). The optional ! removes the marker and enters
" insert mode where it was.
command! -bang -bar -range=0 -nargs=+ Tpl call s:tpl(<count>, [<f-args>], <bang>0)

" :Mde[!] file
" Start editing `file` and pre-populate it with the German meeting minutes
" template. If the current buffer has unsaved changes, this command will
" refuse to abandon them unless the optional ! is given.
command! -bang -bar -complete=file -nargs=1 Mde e<bang> +1Tpl!\ mde <args>
