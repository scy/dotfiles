" If $TERM is not xterm, Vim won't initialize the sequences for using true
" color output (y tho), so let's do it manually.
if !has('nvim')
	let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
	let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif
