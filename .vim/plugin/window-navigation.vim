" Move between windows with CTRL-[hjkl], but in normal mode only.
" This overwrites the useful CTRL-L (for redrawing the screen), which I map to
" <Leader>l instead.
nnoremap <C-H> <C-W><C-H>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <Leader>l <C-L>
