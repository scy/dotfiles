" Don't underline the current line number, the default color is enough. But
" making it bold is nice.
highlight CursorLineNr term=bold cterm=bold

" If the cursor line is highlighted somehow, _only_ highlight the line number,
" not the line itself, if possible, because on some transparent terminals it
" looks bad.
if exists('&cursorlineopt')  " available since Vim 8.1.2019, Neovim 0.6.0
	set cursorlineopt=number
endif

" Once I only had this enabled for Neovim, because I found it to be sluggish
" on Vim, but I've tested it and apparently it's no longer the case or was a
" Windows Terminal specific issue. Therefore, enable cursor line highlighting
" by default.
set cursorline

if &t_Co >= 256
	" If we have a 256-color terminal, use a dark gray background for the
	" current line.
	highlight CursorLine term=underline cterm=NONE ctermbg=235
else
	" If we don't have 256 colors, underline the current line, but don't
	" enable this by default because I find it too distracting.
	highlight CursorLine term=underline cterm=underline ctermbg=NONE
endif
