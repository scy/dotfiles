" Enable syntax highlighting. Note that in Vim, `syntax on` and `syntax
" enable` have slightly different effects: The former will reset any already
" defined highlights to the defaults, the latter won't.
syntax enable
