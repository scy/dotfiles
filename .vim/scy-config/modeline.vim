" Disable modelines for security reasons, see CVE-2019-12735 et al.
" I might enable them again in the future when I feel safe doing so.
" They are off by default on Debian, but "set nocompatible" in the vimrc will
" have turned them on nevertheless.
set nomodeline
