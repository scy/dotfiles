" Enable the mouse and set a useful mouse reporting mode. "sgr" works with
" wezterm, Windows Terminal and Alacritty and is (in contrast to "xterm2") not
" limited to 223 columns. However, 'ttymouse' is not required (and not
" available) in NeoVim.
set mouse=a
if !has('nvim')
	set ttymouse=sgr
endif
