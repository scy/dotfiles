" In the viminfo state file, store file marks and unlimited register contents.
" Search, command line and input history size are defined via 'history'.
" Also, put the viminfo file in ~/.vim, not directly in the home directory.
" In Neovim, 'viminfo' is an alias to 'shada', but viminfo and ShaDa files are
" incompatible. So on a system running both Vim and Neovim, 'viminfo'/'shada'
" should point to different files.
set viminfo='100,h,f1
if has('nvim')
	set shada+=n~/.vim/shada
else
	set viminfo+=n~/.vim/viminfo
endif
set history=100

" Don't litter .swp files all over the place, put them in ~/.vim/swp instead,
" but fall back to "same directory" if that's not possible for some reason.
set directory=~/.vim/swp,.

" What to store in a session file.
" I'd prefer not to store options, but the manual says "many things won't work
" well" if I leave it out.
set sessionoptions=folds,options,sesdir,slash,tabpages,unix,winsize

" Do not allow hidden buffers. This is the default in Vim, but not in Neovim.
" I don't like it when there's unsaved changes that I can't see.
set nohidden
