function! ScyTrunc(str, maxlen)
	" TODO: Move this to a better place.
	return strchars(a:str) > a:maxlen ? strpart(a:str, 0, a:maxlen).'…' : a:str
endfun

" Set terminal window title. Adds a bullet if modified, and `@hostname` if the
" user is connected via SSH.
set title titlestring=%{&modified?'•\ ':''}%{ScyTrunc(expand('%:p:h:t').'/'.expand('%:t'),20)}%{ScyTrunc(empty($SSH_CONNECTION)?'':'@'.hostname(),9)}

" Set the terminal window title when exiting to something nicer than the default.
set titleold=♥\ Vim\ ♥
