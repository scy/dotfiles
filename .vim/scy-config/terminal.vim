" Open a terminal window shell that closes automatically when exiting.
if has('nvim')
	noremap <silent> <Leader>! <Cmd>terminal ++close<CR>
else
	noremap <silent> <Leader>! :terminal ++close<CR>
endif
