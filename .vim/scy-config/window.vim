" When splitting vertically, split to the right (not to the left).
" Similarly, when splitting horizontally, split below (not to the top).
set splitright splitbelow
