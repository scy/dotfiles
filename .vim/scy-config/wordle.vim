" Define a command to note down & commit & push my Wordle solutions for today.
" See <https://codeberg.org/scy/wordle-solutions> for the results.
" This command will:
"   1. Open this month's YAML file and `cd` to its directory.
"   2. Add a new block for today.
"   3. Enter insert mode.
"   4. As soon as I leave insert mode, if I have made any changes, save the
"      file and prepare a command in the command line. Don't directly start it
"      though, give me the option to modify it first.
" The command will define & call a shell function that will commit & push the
" changes. I'm using a function here to have the commit message at the end of
" the line, where it's convenient to edit.
" I'm defining the command as a buffer-local ex command instead of directly
" inlining it in the autocmd. This allows me to call it manually later on if I
" escaped out of the autocmd handling.
command! -bang Wordle
	\ execute "edit<bang>" strftime("~/proj/wordle-solutions/solutions/%Y-%m.yaml")
	\ | silent lcd %:p:h
	\ | execute "command! -buffer Commit w | call feedkeys(':!cnp() { git add *.yaml && git commit -m \"$*\" && git push; }; cnp Add\ ' . strftime('%Y-%m-%d'), 'n')"
	\ | call append(line('$'), ['', strftime('%Y-%m-%d:'), '  '])
	\ | augroup WordleCommit
	\ | execute 'autocmd InsertLeave * if b:changedtick !=' b:changedtick '| execute "Commit" | endif | autocmd! WordleCommit'
	\ | augroup end
	\ | silent $
	\ | redraw
	\ | startinsert!
