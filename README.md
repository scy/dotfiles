# dotfiles

Good Unix tools are configured by files written by the user, so-called dotfiles.
Bad tools are configured by databases and GUIs.
This repository tries to apply my preferred settings to both of them.

## What's configured

* [Alacritty](https://github.com/alacritty/alacritty) (a bit)
* [bash](https://www.gnu.org/software/bash/) environment
* [direnv](https://direnv.net/)
* [Git](https://git-scm.com/)
* [highlight](http://andre-simon.de/doku/highlight/en/highlight.php)
* [i3status](https://i3wm.org/i3status/)
* [Karabiner-Elements](https://github.com/tekezo/Karabiner-Elements)
* keyboard layout on macOS (manually)
* my [Kinesis Advantage 2](https://kinesis-ergo.com/shop/advantage2/)
* [mako](https://github.com/emersion/mako)
* [offlineimap](http://www.offlineimap.org/)
* [OpenSSH](https://www.openssh.com/)
* [sway](https://swaywm.org/) (a bit)
* Terminal.app (manually)
* [Termux](https://termux.com/)
* [tmux](https://tmux.github.io/)
* [unicompose](https://github.com/scy/unicompose)
* [Vim](https://www.vim.org/) and [Neovim](https://neovim.io/) (I try to stay compatible to both, with kind of a progressively enhancing configuration)
* [VisiData](https://www.visidata.org/)
* [Visual Studio Code](https://code.visualstudio.com/)
* [wezterm](https://wezfurlong.org/wezterm/)

## Status

This is currently replacing [dotscy](https://github.com/scy/dotscy) in order to have a fresh start.
The old repo contained a lot of configs for tools that I no longer use, even fonts and binaries that I don't want anymore, but still had in the history.

These settings have been in use on macOS and Debian/Ubuntu (in WSL and standalone); they should run on other Unixes as well.

## Setting up

Instead of symlinking lots of files into your home directory, this repository is supposed to **be** your home directory.
This has the added benefit of you being aware (via `git status`) of new config files that some tool might generate.

The setup procedure will clone the repo into a subdir of your home, then moving everything in it (including `.git`) directly into your home dir.
Files/directories that would be overwritten will be backed up to `~/.orig_home`.

### Termux

Install required (and my favorite) packages and setup storage access.

```sh
pkg install curl git man neovim perl rsync termux-api &&
termux-setup-storage
```

Then, continue as described below in the "Unix" section.

### Ubuntu / Debian

Install required packages.

```sh
sudo apt update            &&
sudo apt install git rsync
```

These might also make sense:

```sh
sudo apt install scdaemon vim
```

On headless machines that are supposed to be used via SSH, it makes sense to forward the `gpg-agent` socket.
However, a remote `gpg-agent` can interfere with that.
If possible, allow SSH to overwrite Unix sockets specified in `RemoteForward` directives by adding the following line to the SSH **server** config:

```
StreamLocalBindUnlink yes
```

**Alternatively,** you can try to disable the remote `gpg-agent` by issuing (as the normal user):

```sh
systemctl --user mask --now gpg-agent.service gpg-agent{,-ssh,-extra,-browser}.socket
```

When I might be working on the machine primarily via the text-only console (e.g. on Raspberry Pis), styling that console might be a good thing, too.
I prefer these settings in `/etc/default/console-setup`:

```
CHARMAP="UTF-8"
CODESET="Uni2"
FONTFACE="Fixed"
FONTSIZE="8x16"
```

Also, to get my [Sihaya](https://github.com/scy/sihaya) color scheme in the console, I add these values to the kernel command line (`/boot/cmdline.txt` on a Pi):

```
vt.default_red=36,207,74,195,0,145,0,205,83,255,112,255,0,197,0,255 vt.default_grn=35,0,166,127,112,0,147,198,81,0,249,166,140,0,220,247 vt.default_blu=32,0,9,0,203,188,165,178,76,0,13,0,255,255,247,226
```

Then, continue as described below in the "Unix" section.

### Fedora

I'm currently using Fedora on my Linux laptop.
Since I've started out with a minimal netinstall, I needed to install quite a few packages manually.
Not all of them are strictly required for these dotfiles (you can probably get by with just `git rsync`), but for what it's worth, this is what I installed:

```sh
sudo dnf install acpi bemenu brightnessctl firefox gammastep git iw iwl7260-firmware NetworkManager-wifi net-tools pcsc-lite pinentry-qt rsync sway syncthing vim wpa_supplicant
```

Then, continue below at "Unix".

### Windows

Install [Git for Windows](https://git-scm.com/download/win).
Then, in a command prompt at your home directory, run

```sh
git clone --recurse-submodules https://codeberg.org/scy/dotfiles.git
```

Afterwards, symlink VS Code's `settings.json` [as described in it](.config/Code/User/settings.json).

### WSL

Make sure you have

```
[automount]
options = fmask=0007,dmask=0007
```

in your `/etc/wsl.conf`.
If you allow read/write for “others”, SSH will refuse to accept configs living on the Windows drive (which they do on my machine, because I’m syncing them on the host side).
If you _disallow_ execute, you won’t be able to call Windows commands from WSL.

### Unix

```sh
which rsync > /dev/null                                              &&
umask 0022 && cd                                                     &&
test ! -e .orig_home                                                 &&
git clone --recurse-submodules https://codeberg.org/scy/dotfiles.git &&
rsync -avb --backup-dir=.orig_home dotfiles/ .                       &&
rm -rf dotfiles
```

**Hint:**
If you'd like to run these commands on a machine you can't copy/paste to, you can extract and run that section of the readme using this piece of `sed` magic:

```sh
curl -sL https://codeberg.org/scy/dotfiles/raw/branch/main/README.md | sed -n '/^### Unix$/,/^```$/ { /```/d; p }' | sh
```

If you have installed a Syncthing package and would like to enable Syncthing for the `scy` user on boot, you might want to do something like:

```sh
sudo systemctl enable --now syncthing@scy
```

## Manual configuration

Although I'm aiming to automate as much as possible with this repo, for some things it's currently too complicated, at least at the moment.
Maybe I'll add automation scripts in the future, but right now it's not worth the effort.

### On machines without HTTPS push to GitHub

On some machines, especially those without full disk encryption, I don’t want to store GitHub credentials directly on disk.
Instead, I configure them to clone (anonymously) via HTTPS, but push via SSH:

```sh
git config -f ~/.gitconfig-local url.ssh://git@github.com/.pushInsteadOf https://github.com/
```

### On a Mac

* Install [Karabiner-Elements](https://github.com/tekezo/Karabiner-Elements).
* Change the **keyboard layout** to US English. Not US International, because this results in dead keys which massively disturb my programming flow.
* In **Terminal.app:**
  * on startup, new window with **Pro**
  * change default profile to **Pro**
  * in the **Window** tab of the profile, change the default size to **160x40**
  * in the **Shell** tab of the profile, change the setting to **close the window when the shell exits without error**

### On Windows

* The default Windows console doesn't provide a visual bell, but the audible one is annoying. Therefore, mash Backspace in a bash for a few times to produce an audible bell. Then, right-click on the volume symbol in the task bar, choose "Open Volume Mixer" and mute "Console Window Host".
* See [`settings.json`](.config/Code/User/settings.json) for instructions on how to symlink the VS Code settings from this repo into `%APPDATA%`.
* If you dual-boot with Linux, you may wish to ask Windows to set the system's real-time clock to UTC (instead of local time). To do that, run the following command in an admin shell: `reg add "HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\TimeZoneInformation" /v RealTimeIsUniversal /d 1 /t REG_QWORD /f`
