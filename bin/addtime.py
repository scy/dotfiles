#!/usr/bin/env python3

from argparse import ArgumentParser
from datetime import timedelta
import re
from typing import List


HMS = re.compile(
    r"^(?:(?:(?P<hours>[0-9]+):)?(?P<minutes>[0-5]?[0-9]):)?"
    r"(?P<seconds>[0-5]?[0-9])$"
)


def add(times: List[timedelta], *, verbose: bool = True) -> timedelta:
    result = sum(times, timedelta())
    if verbose:
        print(
            " + ".join(map(str, times)) +
            " = " + str(result) +
            f" ({result.total_seconds() / 3600 :.2f} h)"
        )
    return result


def parse_timespan(text: str) -> timedelta:
    match = HMS.match(text)
    if match:
        params = {
            k: int(v)
            for k, v in match.groupdict("0").items()
        }
        return timedelta(**params)
    raise ValueError(f"'{text}' could not be parsed")


def cli():
    parser = ArgumentParser(
        description="Add timespans together.",
    )
    parser.add_argument(
        "timespan",
        nargs="+",
        type=parse_timespan,
    )

    args = parser.parse_args()

    add(args.timespan)


# Integrated tests. Run with `pytest --pyargs PATH_TO_THIS_FILE`.
try:
    import pytest

    @pytest.mark.parametrize("input,expected", [
        ("23", timedelta(seconds=23)),
        ("5:23", timedelta(seconds=23, minutes=5)),
        ("5:0:23", timedelta(seconds=23, hours=5)),
    ])
    def test_parser(input: str, expected: timedelta):
        assert parse_timespan(input) == expected

    @pytest.mark.parametrize("input,expected", [
        ([
            timedelta(hours=1, minutes=6, seconds=29),
            timedelta(minutes=32, seconds=24),
        ], timedelta(hours=1, minutes=38, seconds=53)),
    ])
    def test_addition(input: List[timedelta], expected: timedelta):
        assert add(input, verbose=False) == expected
except ImportError:
    pass


if __name__ == "__main__":
    cli()
