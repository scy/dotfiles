#!/usr/bin/env python3

from http.client import HTTPSConnection
import json


def get_version() -> str:
    host = "codeberg.org"
    con = HTTPSConnection(host)
    con.request("GET", "/api/v1/version", headers={
        "Host": host,
        "Accept": "application/json",
    })
    res = con.getresponse()
    body = res.read()
    if res.status < 200 or res.status >= 400:
        raise Exception(f"unexpected response: {res.status}: {body}")
    data = json.loads(body)
    return data["version"]


if __name__ == "__main__":
    print(get_version())
