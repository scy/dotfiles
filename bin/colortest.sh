#!/bin/sh
# Yet Another Color Table Script
# by Tim Weber, https://codeberg.org/scy/dotfiles

#  $1  color number
color_name() {
	case "$1" in
		0) echo 'black';;
		1) echo 'red';;
		2) echo 'green';;
		3) echo 'yellow';;
		4) echo 'blue';;
		5) echo 'magenta';;
		6) echo 'cyan';;
		7) echo 'white';;
		9) echo 'default';;
		*) printf '%s\n' "$1";;
	esac
}

#  $1  background color (0-7 or 9 for "default")
#  $2  base foreground color (0-7 or 9 for "default")
#  $3  "bold" or "bright" flag
print_cell() {
	code="3${2};4${1}"
	[ "$3" = 'bold' ] && code="1;$code"
	[ "$3" = 'bright' ] && code="9${2};10${1}"
	printf '\033[%sm %8s \033[0m ' "$code" "$code"
}

#  $1  base foreground color for this line (0-7 or 9 for "default")
#  $2  "bold" or "bright" flag
print_line() {
	name="$(color_name "$1")"
	one=''
	fgdigit='3'
	bgdigit='4'
	[ "$2" = 'bold' ] && name="bold $name" && one='1;'
	[ "$2" = 'bright' ] && name="bright $name" && fgdigit='9' && bgdigit='10'
	printf '\033[%s%d%d;%d9m%14s\033[0m  ' "$one" "$fgdigit" "$1" "$bgdigit" "$name"
	for bg in 9 0 1 2 3 4 5 6 7; do
		print_cell "$bg" "$1" "$2"
	done
}

# Header line.
printf '%14s  '  # space for color name column
for color in 9 0 1 2 3 4 5 6 7; do
	printf '\033[3%d;49m %8s \033[0m ' "$color" "$(color_name "$color")"
done
printf '\n'

# Actual color table.
for flag in '' bright bold; do
	for fg in 9 0 1 2 3 4 5 6 7; do
		print_line "$fg" "$flag"
		printf '\n'
	done
done

# 256-color mode.
printf '\n16 base colors in 256-color mode: '
i=0; while [ "$i" -le 15 ]; do
	printf '\033[48;5;%dm  \033[0m' "$i"
	i=$((i + 1))
done
printf '\n\n216-color cube:\n'
r=0; while [ "$r" -lt 6 ]; do
	g=0; while [ "$g" -lt 6 ]; do
		b=0; while [ "$b" -lt 6 ]; do
			printf '\033[48;5;%dm  \033[0m' $((16 + 36 * r + 6 * g + b))
			b=$((b + 1))
		done
		printf '  '
		g=$((g + 1))
	done
	printf '\n'
	r=$((r + 1))
done
printf '\n24-step grayscale: '
i=232; while [ "$i" -le 255 ]; do
	printf '\033[48;5;%dm  \033[0m' "$i"
	i=$((i + 1))
done
printf '\n'
