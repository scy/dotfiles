#!/bin/sh
set -e

ME='dashcam-play.sh'

msg() {
	printf '%s: %s\n' "$ME" "$*"
}

err() {
	msg "$*" >&2
}

die() {
	rc="$1"; shift
	err "$*"
	exit "$rc"
}

launch_mpv() {
	if [ "$#" -ne 3 ]; then
		die 3 'could not find 3 related files'
	fi
	mpv "$1" --external-file="$2" --external-file="$3" --lavfi-complex='[vid1]scale=1920:-1[front];[front][vid2][vid3]vstack=inputs=3[vo]'
}

if [ "$#" -ne 1 ]; then
	die 1 'expecting exactly one argument'
fi

stem="${1%_*[FRI].MP4}"
msg "file stem detected as $stem"

ts="$(date -d "$(printf '%s' "$stem" | sed -ne 's/^\([0-9]\{4\}\)_\([0-9]\{2\}\)\([0-9]\{2\}\)_\([0-9]\{2\}\)\([0-9]\{2\}\)\([0-9]\{2\}\)$/\1-\2-\3 \4:\5:\6/p') UTC" +%s)"
if [ -z "$ts" ]; then
	die 2 'could not extract timestamp'
fi
msg "timestamp detected as $(date -R -d "@$ts")"

launch_mpv $(for cam in F R I; do
	for offset in 0 1 -1 2 -3; do
		prefix="$(date -u -d "@$((ts + offset))" '+%Y_%m%d_%H%M%S_')"
		for suffix in "$cam.MP4" "P$cam.MP4"; do
			file="$prefix$suffix"
			if [ -e "$file" ]; then
				printf '%s\n' "$file"
			fi
		done
	done
done)
