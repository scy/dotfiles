#!/bin/bash

test_vimlike() {
	echo "Testing $1 ..."
	timeout --foreground -k 35 30 "$1" -n -m -c 'if v:errmsg == "" | qa! | else | cq | endif'
	rc="$?"
	# reset  # Vim can leave the terminal in a messed-up state.
	return "$rc"
}

collect_result() {
	rc="$?"
	echo "$1 returned $rc."
	if [ "$rc" -eq 0 ]; then
		success="$success$1 "
	else
		fail="$fail$1:$rc "
	fi
}

success=''
fail=''

test_vimlike vim
collect_result vim

test_vimlike nvim
collect_result nvim

# Killing Vim while it's starting up can mess up the terminal.
reset

if [ "$success" != '' ]; then
	printf 'success: %s\n' "$success"
fi
if [ "$fail" != '' ]; then
	printf 'failure: %s\n' "$fail"
	exit 1
fi

exit 0
