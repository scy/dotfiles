#!/bin/sh
set -e

# Find a sensible editor and use that to edit the file(s) supplied.

# Check whether Neovim is available, and if not, install it.
nvim_version='v0.10.3'
nvim_sha256='be189915a2a0da3615576e2db06a7c714aef0ae926b4da6107e589a3cc623e5c'
nvim_dir="neovim-$nvim_version"
full_nvim_dir="$HOME/bin/$nvim_dir"
nvim_symlink="$HOME/bin/nvim"
tarball='nvim-linux64.tar.gz'
if ! [ -d "$full_nvim_dir" ] && [ "$(uname -m)" = 'x86_64' ]; then
	echo 'edit: trying to fetch Neovim ...' >&2
	tmpfile="$(mktemp --tmpdir "neovim-$nvim_version.XXXXXXXXXX.tar.gz")"
	curl -L -o "$tmpfile" "https://github.com/neovim/neovim/releases/download/$nvim_version/$tarball"
	nvim_actual_sha256="$(sha256sum "$tmpfile" | cut -d ' ' -f 1)"
	if [ "$nvim_sha256" = "$nvim_actual_sha256" ]; then
		echo 'edit: extracting Neovim ...'
		mkdir -p "$full_nvim_dir"
		tar -x -C "$full_nvim_dir" --strip-components 1 -f "$tmpfile"
		ln -sf "$nvim_dir/bin/nvim" "$nvim_symlink"
		rm "$tmpfile"
	else
		echo "Neovim tarball SHA256 mismatch: expected $nvim_sha256, got $nvim_actual_sha256!"
		exit 2
	fi
fi

for editor in nvim vim vi nano; do
	if command -v "$editor" >/dev/null 2>&1; then
		exec "$editor" "$@"
	fi
done

printf 'edit: no sensible editor found on this system.\n' >&2
exit 1
