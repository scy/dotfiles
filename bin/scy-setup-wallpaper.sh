#!/bin/sh
set -e

WALLPAPER_FILE="$HOME/.wallpaper"
FALLBACK_COLOR='242320'  # Sihaya's background color.

wallpaper_exists() {
	test -r "$WALLPAPER_FILE"
}

if [ -n "$SWAYSOCK" ]; then
	# Running under Sway.
	swaymsg output '*' background "$WALLPAPER_FILE" fill "#$FALLBACK_COLOR"
fi
