:: Wrapper to make VS Code (and probably other tools) use the ssh binary and
:: config present inside of WSL.
:: As suggested in <https://stackoverflow.com/a/66048792/417040>.

C:\Windows\system32\wsl.exe ssh %*
